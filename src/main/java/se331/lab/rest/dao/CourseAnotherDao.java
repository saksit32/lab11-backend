package se331.lab.rest.dao;

import se331.lab.rest.entity.Course;

import java.util.List;

public interface CourseAnotherDao {
    List<Course> findAll();
    Course findById(Long id);
    Course save(Course course);
}
