package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;
    LecturerAnotherDao lecturerAnotherDao;

    @Override
    @Transactional
    public List<Course> getCourseWhichStudentEnrolledMoreThan(int amountOfStudent) {
        List<Course> courses = courseAnotherDao.findAll();
        return courses.stream().filter(course -> course.getStudents().size() > amountOfStudent).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<Course> getCourses() {
        List<Course> courses = courseAnotherDao.findAll();
        log.info(courses.get(1).getLecturer() + "");
        return courses;
    }

    @Override
    @Transactional
    public Course getCourseById(Long id) {
        Course course = courseAnotherDao.findById(id);
        return course;
    }

    @Override
    @Transactional
    public Course save(Course course) {
        Lecturer lecturer = lecturerAnotherDao.getLecturerById(course.getLecturer().getId());
        course.setLecturer(lecturer);
        return courseAnotherDao.save(course);
    }
}